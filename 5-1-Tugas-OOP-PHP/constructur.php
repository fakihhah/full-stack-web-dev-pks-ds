<?php

class Hewan {
    public $nama = "Harimau";
    public $jenis = "Karnivora";

    public function __construct($nama , $jenis)
    {
        $this->nama = $nama;
        $this->jenis = $jenis;
        echo "Ini adalah " . $this->nama . " , dia adalah : " . $this->jenis;

    }

}    
    
$tikus = new Hewan("Tikus" , "Omnivora");
echo "<br>";
$anjing = new Hewan("Anjing" , "Omnivora");

?>